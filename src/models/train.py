import click
import mlflow
import pandas as pd
import numpy as np
import mlflow.sklearn
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from mlflow.models import infer_signature
from sklearn.metrics import classification_report


@click.command()
@click.option("--train_path", required=True, help="Input file path", type=click.Path())
@click.option("--test_path", required=True, help="Input file path", type=click.Path())
@click.option("--mlflow_url", required=True, help="MLflow tracking URI", type=str)
@click.option("--experiment_name", required=True, help="MLflow experiment name", type=str)
def train_model(train_path: str, test_path: str, mlflow_url: str, experiment_name: str) -> None:
    train = pd.read_csv(train_path)
    test = pd.read_csv(test_path)

    x_train = train.drop("Target", axis=1)
    y_train = train["Target"].astype(int)
    x_test = test.drop("Target", axis=1)
    y_test = test["Target"].astype(int)

    mlflow.set_tracking_uri(mlflow_url)
    mlflow.set_experiment(experiment_name)

    with mlflow.start_run():
        pipeline = Pipeline([
            ('scaler', StandardScaler()),  # номализация фичей
            ('classifier', LogisticRegression())
        ])

        param_grid = {
            'classifier__C': [0.01, 0.1, 0.5],
            'classifier__l1_ratio': np.linspace(0, 1, 3),
            'classifier__class_weight': ['balanced', None],
            'classifier__random_state': [42],
            'classifier__penalty': ['elasticnet'],
            'classifier__solver': ['saga']
        }

        # Найдем лучише параметры гридсерчем
        grid_search = GridSearchCV(pipeline,
                                   param_grid,
                                   cv=5,
                                   scoring='f1')

        # Fit the model
        grid_search.fit(x_train, y_train)

        y_pred = grid_search.predict(x_test)
        metrics = classification_report(y_test, y_pred, output_dict=True)

        mlflow.log_param("grid_params", grid_search.best_params_)
        mlflow.log_param("best_f1_score", grid_search.best_score_)
        mlflow.log_param("f1_score", metrics["weighted avg"]["f1-score"])
        mlflow.log_param("precision", metrics["weighted avg"]["precision"])
        mlflow.log_param("recall", metrics["weighted avg"]["recall"])

        predictions = grid_search.predict(x_train)
        signature = infer_signature(x_train, predictions)

        mlflow.sklearn.log_model(
            sk_model=grid_search,
            artifact_path="model",
            signature=signature,
            registered_model_name=experiment_name,
        )


if __name__ == "__main__":
    train_model()

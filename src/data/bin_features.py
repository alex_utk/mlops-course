import click
import pandas as pd


@click.command()
@click.option("--input_path", "-i", required=True, help="Path to the input data", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Path to the output data", type=click.Path())
def bin_features(input_path: str, output_path: str) -> None:
    data = pd.read_csv(input_path)
    bins = [
        (0, 18, 0),
        (18, 27, 1),
        (27, 45, 2),
        (45, 66, 3),
        (66, float("inf"), 4),
    ]
    for lower, upper, label in bins:
        data.loc[(data['Age'] > lower) &
                 (data['Age'] <= upper), "Age_bin"] = label
    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    bin_features()

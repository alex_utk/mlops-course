import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.option("--input_path", "-i", required=True, help="Path to the input data", type=click.Path())
@click.option("--train_path", required=True, help="Path to the output train data", type=click.Path())
@click.option("--test_path", required=True, help="Path to the output test data", type=click.Path())
def split_data(input_path: str, train_path: str, test_path: str) -> None:
    data = pd.read_csv(input_path)
    train, test = train_test_split(data, test_size=0.2,
                                   random_state=0,
                                   stratify=data['Target'])
    train.to_csv(train_path, index=False)
    test.to_csv(test_path, index=False)


if __name__ == "__main__":
    split_data()

import click
import pandas as pd


@click.command()
@click.option("--input_path", "-i", required=True, help="Path to the input data", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Path to the output data", type=click.Path())
def rename_target(input_path: str, output_path: str) -> None:
    data = pd.read_csv(input_path)
    data = data.rename(columns={'Outcome': 'Target'})
    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    rename_target()

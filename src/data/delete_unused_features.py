import click
import pandas as pd


@click.command()
@click.option("--input_path", "-i", required=True, help="Path to the input data", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Path to the output data", type=click.Path())
def delete_unused_features(input_path: str, output_path: str) -> None:
    data = pd.read_csv(input_path)
    data = data.drop(columns=['SkinThickness', 'Age'])
    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    delete_unused_features()

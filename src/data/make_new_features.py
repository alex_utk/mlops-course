import click
import pandas as pd


@click.command()
@click.option("--input_path", "-i", required=True, help="Path to the input data", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Path to the output data", type=click.Path())
def make_new_features(input_path: str, output_path: str) -> None:
    data = pd.read_csv(input_path)
    data['Glucose_Dynamics'] = 126 - 0.5 * data["Glucose"] * data['Insulin']
    data['Insulin'] = data['Insulin'] / 23.3 - 67.1
    data['BloodPressure^2'] = data['BloodPressure'] ** 2
    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    make_new_features()
